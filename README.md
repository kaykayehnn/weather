# Typescript React Redux boilerplate

### What's included:
- Typescript
- TSLint configured with Standard Style settings
- React & Redux
- Redux DevTools via Chrome extension
- Webpack configuration with Hot Module Replacement
- CSS Modules

### Commands

Install dependencies
```bash
npm install
```

Run development server
```bash
npm start
```

Build for production:
```bash
npm run build
```

### Bundle sizes
Size of emitted files after minification and GZIP compression:
 - main.js - 1.4K
 - polyfills.js - 4.9K
 - vendor.js - 38K

Total: 44.3K