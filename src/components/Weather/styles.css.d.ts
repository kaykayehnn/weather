export const title: string;
export const element: string;
export const city: string;
export const weatherIcon: string;
export const description: string;
export const stats: string;
export const stat: string;
export const icon: string;
