import React, { Component, Fragment } from 'react'
import { Card } from '@material-ui/core'
import Select from 'react-select'

import { WeatherProps } from '../../containers/WeatherContainer'
import * as styles from './styles.css'
import cities from './cities'
import { ActionMeta } from 'react-select/lib/types'

const DEFAULT_CITY = 'Sofia'

interface WeatherState {
  isLoading: boolean
}

export class Weather extends Component<WeatherProps, WeatherState> {
  constructor (props: WeatherProps) {
    super(props)

    this.state = { isLoading: true }

    this.onChange = this.onChange.bind(this)
  }

  componentDidMount () {
    this.setState({ isLoading: true })

    this.fetchCity(DEFAULT_CITY)
      .then(() => this.setState({ isLoading: false }))
  }

  fetchCity (city: string) {
    return (this.props.fetchData(city) as any as Promise<any>)
  }

  onChange (value: any, { action }: ActionMeta) {
    if (action === 'select-option') {
      this.fetchCity(value.value)
    }
  }

  render () {
    const data = this.props.data[0]
    const { isLoading } = this.state

    let card
    if (!isLoading) {
      card = (
        <Card className={styles.element}>
          <div className={styles.city}>{data.city_name}</div>
          <img className={styles.weatherIcon} src={`https://www.weatherbit.io/static/img/icons/${data.weather.icon}.png`}></img>
          <div className={styles.description}>{data.weather.description}</div>
          <div className={styles.stats}>
            <div className={styles.stat}>
              <img className={styles.icon} src='./vectors/sunrise.svg'></img>
              <div>{data.sunrise}</div>
            </div>
            <div className={styles.stat}>
              <img className={styles.icon} src='./vectors/temperature.svg'></img>
              <div>{data.temp}°</div>
            </div>
            <div className={styles.stat}>
              <img className={styles.icon} src='./vectors/wind.svg'></img>
              <div>{data.wind_spd.toFixed(2)}</div>
            </div>
            <div className={styles.stat}>
              <img className={styles.icon} src='./vectors/sunset.svg'></img>
              <div>{data.sunset}</div>
            </div>
          </div>
        </Card>)
    }

    return (
      <Fragment>
        <div className={styles.title}>Weather</div>
        <Select className={styles.element}
          options={cities}
          isLoading={isLoading}
          defaultInputValue={DEFAULT_CITY}
          onChange={this.onChange}></Select>
        {card}
      </Fragment>
    )
  }
}
