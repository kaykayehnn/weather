import { MapDispatchToProps } from 'react-redux'

export type MapDispatchToProps<TDispatchProps, TOwnProps = {}> =
  MapDispatchToProps<TDispatchProps, TOwnProps>
