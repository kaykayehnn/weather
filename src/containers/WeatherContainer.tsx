import { connect } from 'react-redux'

import { Weather } from '../components/Weather'
import { fetchData } from '../store/actions/weather'
import { MapStateToProps } from '../types/MapStateToProps'
import { MapDispatchToProps } from '../types/MapDispatchToProps'
import { WeatherState } from '../store/state/WeatherState'

interface PropsFromState extends WeatherState {
}

interface PropsFromDispatch {
  fetchData: typeof fetchData
}

export type WeatherProps = PropsFromState & PropsFromDispatch

const mapStateToProps: MapStateToProps<PropsFromState> =
  (state) => state.weather

const mapDispatchToProps: MapDispatchToProps<PropsFromDispatch> = {
  fetchData
}

export const WeatherContainer = connect(mapStateToProps, mapDispatchToProps)(Weather)
