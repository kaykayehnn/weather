import { combineReducers } from 'redux'

import { weatherReducer } from './weather'
import { AppState } from '../state/AppState'

export const rootReducer = combineReducers<AppState>({
  weather: weatherReducer
})
