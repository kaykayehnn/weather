import { WeatherState } from '../state/WeatherState'
import { WeatherActions, FETCH_DATA_SUCCESS } from '../actions/weather'

const initial: WeatherState = {
  data: [],
  count: 0
}

export function weatherReducer (state: WeatherState = initial, action: WeatherActions) {
  switch (action.type) {
    case FETCH_DATA_SUCCESS:
      return action.data
    default:
      return state
  }
}
