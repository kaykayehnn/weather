export interface WeatherState {
  data: Datum[]
  count: number
}

export interface Datum {
  wind_cdir: string
  rh: number
  pod: string
  lon: number
  pres: number
  timezone: string
  ob_time: string
  country_code: string
  clouds: number
  vis: number
  state_code: string
  wind_spd: number
  lat: number
  wind_cdir_full: string
  slp: number
  datetime: string
  ts: number
  station: string
  h_angle: number
  dewpt: number
  uv: number
  dni: number
  wind_dir: number
  elev_angle: number
  ghi: number
  dhi: number
  precip: number
  city_name: string
  weather: Weather
  sunset: string
  temp: number
  sunrise: string
  app_temp: number
}

export interface Weather {
  icon: string
  code: number
  description: string
}
