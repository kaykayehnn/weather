import { WeatherState } from './WeatherState'

export interface AppState {
  readonly weather: WeatherState
}
