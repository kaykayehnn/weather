import { Action } from 'redux'

import { ThunkAction } from '../../types/ThunkAction'
import { WeatherState } from '../state/WeatherState'

export const FETCH_DATA_SUCCESS = 'FETCH_WEATHER_DATA'

export interface FetchDataAction extends Action<typeof FETCH_DATA_SUCCESS> {
  data: WeatherState
}

export type WeatherActions = FetchDataAction

export function fetchSuccess (data: WeatherState): FetchDataAction {
  return { type: FETCH_DATA_SUCCESS, data }
}

export function fetchData (city: string): ThunkAction<Promise<any>> {
  return (dispatch) => {
    return fetch(`https://api.weatherbit.io/v2.0/current?key=acee80cdd59342a2a19fbd4078e69984&lang=bg&city=${city}`)
      .then(res => res.json())
      .then(data => {
        return dispatch(fetchSuccess(data))
      })
  }
}
