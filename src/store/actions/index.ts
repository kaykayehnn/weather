import { WeatherActions } from './weather'

export type AppActions = WeatherActions
